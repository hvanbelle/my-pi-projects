#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import * 
from time import sleep, strftime
from datetime import datetime

lcd = Adafruit_CharLCD()

#cmd = 'ssh -l hvanbelle -i /home/pi/.ssh/id_rsa linux-mxe6 "cat /home/hvanbelle/my-projects/project_sensors_cosm/temp_vals.txt"'
cmd = 'curl -s linux-mxe6.lan.vanbellenet.be:/sensors/temp_vals.txt'

screen_timer = 2 # seconds between screen refresh new info

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

lcd.clear()
while 1:
    lcd.message(datetime.now().strftime('%b %d  %H:%M:%S\n'))
    sleep(screen_timer)
    
    lcd.clear()    
    temp_vals = run_cmd(cmd).split("\n")
    
    temp_vals.pop()
    count = 0
    for t in temp_vals:
        count = count + 1
        lcd.message('T%s: %s\n' % ( count, t ) )
        sleep(screen_timer)
        lcd.clear()
    