#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import * 
from time import sleep, strftime
from datetime import datetime

lcd = Adafruit_CharLCD()

cmd = "cat /etc/hosts | grep 192.168.2"

screen_timer = 5 # 5 seconds between screen refresh new info

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output

while 1:
    lcd.clear()
    lcd.message(datetime.now().strftime('%b %d  %H:%M:%S\n'))
    sleep(screen_timer)
    
    lcd.clear()
    ip_host = run_cmd(cmd).split("\n")
    for i in ip_host:
        ip = i.split("\t")        
        count = 0
        for j in ip:
            count = count + 1
            if (count == 1):
                lcd.message('%s\n' % ( j ) )
            else:
                lcd.message('%s' % ( j ) )
                count = 0
                sleep(screen_timer)
                lcd.clear()
                
    